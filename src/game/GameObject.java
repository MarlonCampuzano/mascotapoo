/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 *
 * @author gaby
 */
public class GameObject {
    
    private Node view;
    private Point2D velocity = new Point2D(0, 0);
    private boolean alive = true;
    
    //PROPIEDADES SOLO PARA LOS TESOROS
    private double money = 0;
    private boolean bomb = false;

    public GameObject(Node view) {
        this.view = view;
    }
    
    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public boolean isBomb() {
        return bomb;
    }

    public void setBomb(boolean bomb) {
        this.bomb = bomb;
    }

    public Node getView() {
        return view;
    }

    public void setView(Node view) {
        this.view = view;
    }

    public Point2D getVelocity() {
        return velocity;
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isDead() {
        return !alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public double getRotate() {
        return view.getRotate();
    }
    
    public void update() {
    view.setTranslateX(view.getTranslateX() + velocity.getX());
    view.setTranslateY(view.getTranslateY() + velocity.getY());

    }
    
    public void moveRight() {
        view.setTranslateX(view.getTranslateX() + 5);
    }

    
    public void moveLeft() {
        view.setTranslateX(view.getTranslateX() - 5);
    }

    //VALIDA COLISION ENTRE OBJETOS
    public boolean isColliding(GameObject other) {
        return getView().getBoundsInParent().intersects(other.getView().getBoundsInParent());
    }    
}
