/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Point2D;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import mascotapoo.User;

/**
 *
 * @author Frank, Gabriela, Marlon
 */
public class Game extends Application {

    private Pane root;

    Image gameOverImg = new Image("GAMEOVER.jpg");
    Image petGame = new Image("petGame.jpg");
    Image treasure = new Image("treasure.png");
    Image bulletImg = new Image("bullet.png");

    private List<GameObject> bullets = new ArrayList<>();
    private List<GameObject> enemies = new ArrayList<>();

    private GameObject player;

    private double moneyBank = 0;
    private boolean gameOver = false;

    
    
    private ImageView setImg(Image img, int height) {
        ImageView iv = new ImageView(img);
        iv.setFitHeight(40);
        iv.setPreserveRatio(true);
        return iv;
    }

    public double getMoneyBank() {
        return moneyBank;
    }

    public void setMoneyBank(double moneyBank) {
        this.moneyBank = moneyBank;
    }

    private Parent createContent() {

        root = new Pane();
        root.setPrefSize(600, 600);

        player = new Player();
        player.setVelocity(new Point2D(0, 0));

        addGameObject(player, 300, 25);

        AnimationTimer timer = new AnimationTimer() {

            @Override
            public void handle(long now) {
                onUpdate();
            }

        };
        timer.start();

        return root;
    }
    
//CREA LOS PROYECTILES
    private void addBullet(GameObject bullet, double x, double y) {
        bullets.add(bullet);
        addGameObject(bullet, x, y);
    }

    
    //CREA LOS TESOROS
    private void addEnemy(GameObject enemy, double x, double y) {
        enemies.add(enemy);
        addGameObject(enemy, x, y);
    }

    private void addGameObject(GameObject object, double x, double y) {
        object.getView().setTranslateX(x);
        object.getView().setTranslateY(y);

        root.getChildren().add(object.getView());
    }

    //MANEJADOR DE LA ANIMACION
    private void onUpdate() {
        for (GameObject bullet : bullets) {
            for (GameObject enemy : enemies) {
                if (bullet.isColliding(enemy)) {
                    bullet.setAlive(false);
                    enemy.setAlive(false);

                    this.moneyBank += enemy.getMoney();
                    System.out.println(this.moneyBank);

                    root.getChildren().removeAll(bullet.getView(), enemy.getView());

                    if (enemy.isBomb()) {

                        gameOver = true;
                        System.out.println("EXPLOTASTE");
                        root.getChildren().add(new ImageView(gameOverImg));
                        try {
                            this.stop();
                        } catch (Exception ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                }
            }
        }

        bullets.removeIf(GameObject::isDead);
        enemies.removeIf(GameObject::isDead);

        bullets.forEach(GameObject::update);
        enemies.forEach(GameObject::update);

        player.update();

        //GENERA TESOROS ALEATORIAMENTE
        if (Math.random() < 0.0081 && !gameOver) {
            addEnemy(new Enemy(), Math.random() * root.getPrefWidth(), Math.random() * root.getPrefHeight() + 60);
        }

    }

    //FUNCION PARA DISPARAR
    private void shoot() {

        if (!gameOver) {
            Bullet bullet = new Bullet();
            bullet.setVelocity(new Point2D(0, 5));
            addBullet(bullet, player.getView().getTranslateX(), player.getView().getTranslateY());
        }
    }

    private class Player extends GameObject {

        Player() {
            super(setImg(petGame, 20));

        }

    }

    private class Enemy extends GameObject {

        Enemy() {
            super(setImg(treasure, 20));
            int a = (int) (Math.random() * 11);
            this.setMoney(a);
            //System.out.println(a);
            if (a > 9) {
                this.setBomb(true);
                System.out.println("aparecio una bomba");
            }

        }

    }

    private class Bullet extends GameObject {

        Bullet() {
            super(setImg(bulletImg, 30));

        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(createContent()));

        //MOVIMIENTO Y DISPARO CON TECLADO
        stage.getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.RIGHT) {
                player.moveRight();
            } else if (e.getCode() == KeyCode.LEFT) {
                player.moveLeft();
            } else if (e.getCode() == KeyCode.SPACE) {
                shoot();
            }
        });

        //MOVIMIENTO CON MOUSE
        stage.getScene().setOnMouseMoved(e -> {
            player.getView().setTranslateX(e.getX());
        });

        //DISPARO CON MOUSE
        stage.getScene().setOnMouseClicked(e -> {
            shoot();
        });

        stage.setResizable(false);
        stage.show();
    }

    public static void maion(String[] args) {
        launch(args);
    }

}

