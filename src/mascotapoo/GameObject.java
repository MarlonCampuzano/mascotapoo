/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 *
 * @author gaby
 */
public class GameObject {
    private Node view;
    private Point2D velocity = new Point2D(0, 0);

    private boolean alive = true;

    public GameObject(Node view) {
        this.view = view;
    }

    public void update() {
        view.setTranslateX(view.getTranslateX() + velocity.getX());
        view.setTranslateY(view.getTranslateY() + velocity.getY());
    }

    public Point2D getVelocity() {
        return velocity;
    }

    public Node getView() {
        return view;
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
    
    public boolean isDead(){
    return !alive;
    }

    public double getRotate() {
        return view.getRotate();
    }

    public void rotateRight() {
        view.setRotate(view.getRotate() + 5);
    }

    public void rotateLeft() {
        view.setRotate(view.getRotate() - 5);
    }
    
    public boolean isColliding(GameObject other){
    return getView().getBoundsInParent().intersects(other.getView().getBoundsInParent());
    }
}
