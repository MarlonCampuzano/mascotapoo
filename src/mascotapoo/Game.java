/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

import java.util.ArrayList;
import java.util.List;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author IKAROS
 */
public class Game extends Application{
    
    private Stage gameStage;
    private List<GameObject> arrows = new ArrayList<>();
    private List<GameObject> treasures = new ArrayList<>();
    private GameObject player;
    private Pane root;
    
    public Game(User user) throws Exception {
        start(gameStage);
    }
    
    private Parent createContent() {
        root = new Pane();
        root.setPrefSize(600, 600);

        player = new Player();
        player.setVelocity(new Point2D(1, 0));

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                onUpdate();
            }
        };

        return root;
    }
    
    private void addArrow(GameObject arrow, double x, double y) {
        arrows.add(arrow);
        addGameObject(arrow, x, y);
    }

    private void addTreasure(GameObject treasure, double x, double y) {
        treasures.add(treasure);
        addGameObject(treasure, x, y);
    }
    
    private void addGameObject(GameObject object, double x, double y) {
        object.getView().setTranslateX(x);
        object.getView().setTranslateY(y);
        root.getChildren().add(object.getView());
    }
    
    private void onUpdate() {
        for (GameObject arrow : arrows) {
            for (GameObject treasure : treasures) {
                if (arrow.isColliding(treasure)) {
                    arrow.setAlive(false);
                    treasure.setAlive(false);
                }
            }
        }
        arrows.removeIf(GameObject::isDead);
        treasures.removeIf(GameObject::isDead);

        arrows.forEach(GameObject::update);
        treasures.forEach(GameObject::update);
        
        player.update();
        
        if(Math.random()<0.01){
        addTreasure(new Treasure(), Math.random()*root.getPrefWidth(), Math.random()*root.getPrefHeight());
        }
    }

    @Override
    public void start(Stage Stage) throws Exception {
       Stage = new Stage();
       Stage.setScene(new Scene(createContent()));
       Stage.setTitle("Juego");
       Stage.show();
    }
    
    private static class Player extends GameObject {

        Player() {
            super(new Rectangle(40, 20, Color.BLUE));
        }

    }

    private static class Treasure extends GameObject {

        Treasure() {
            super(new Rectangle(40, 20, Color.RED));
        }

    }

    private static class Arrow extends GameObject {

        Arrow() {
            super(new Rectangle(40, 20, Color.GREEN));
        }

    }
    
    
    
}
