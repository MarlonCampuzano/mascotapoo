/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

/**
 *
 * @author Gaby, Marlon, Frank
 */
public class Mascota {
    private String name = "";
    private String b_date = "";
    private String b_hour = "";
    private double age = 0;
    private double hp = 0;
    private double feed = 0;
    private double happiness = 0;
    private double money = 0;
    private double clean = 0;

    //Previene que solo se disminuya 100HP una vez por stat
    private String ifFeed = "";
    private String ifClean = "";
    private String ifHappiness = "";

    public String getIfFeed() {
        return ifFeed;
    }

    public String getIfClean() {
        return ifClean;
    }

    public String getIfHappiness() {
        return ifHappiness;
    }

    public void setIfFeed(String ifFeed) {
        this.ifFeed = ifFeed;
    }

    public void setIfClean(String ifClean) {
        this.ifClean = ifClean;
    }

    public void setIfHappiness(String ifHappiness) {
        this.ifHappiness = ifHappiness;
    }
    
    
    //Creacion de la Mascota :v
    public Mascota(String name, String b_date, String b_hour, double age, double hp, double feed, double happiness, double money, double clean) {
        this.name = name;
        this.b_date = b_date;
        this.b_hour = b_hour;
        this.age = age;
        this.hp = hp;
        this.feed = feed;
        this.happiness = happiness;
        this.money = money;
        this.clean = clean;
        this.ifFeed = "t";
        this.ifClean = "t";
        this.ifHappiness = "t";
    }

    public String getName() {
        return name;
    }

    public String getB_date() {
        return b_date;
    }

    public String getB_hour() {
        return b_hour;
    }

    public double getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
        if (this.hp < 0){this.hp = 0;}
        if (this.hp >500){this.hp = 500;}
    }

    public double getFeed() {
        return feed;
    }

    public void setFeed(double feed) {
        this.feed = feed;
        if (this.feed < 0){this.feed = 0;}
        if (this.feed >10){this.feed = 10;}
    }

    public double getHappiness() {
        return happiness;
    }

    public void setHappiness(double happiness) {
        this.happiness = happiness;
        if (this.happiness < 0){this.happiness = 0;}
        if (this.happiness >10){this.happiness = 10;}
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
        if (this.money < 0){this.money = 0;}
    }

    public double getClean() {
        return clean;
    }
   
    public void setClean(double clean) {
        this.clean = clean;
        if (this.clean < 0){this.clean = 0;}
        if (this.clean >10){this.clean = 10;}
    }
    
    //Disminuye cada stat
    public void DecreaseStats() {
        this.feed -= 1;
        this.happiness -= 1;
        this.clean -= 1;
        if (this.feed < 0) {
            this.feed = 0;
        }
        if (this.happiness < 0) {
            this.happiness = 0;
        }
        if (this.clean < 0) {
            this.clean = 0;
        }
        System.out.println(this.feed + " " + this.happiness + " " + this.clean);
    }
    
    //Aumenta la edad de la mascota y disminuye poco su salud
    public void Birthday() {
        this.age++;
        this.hp--;
        if (this.hp < 0) {
            this.hp = 0;
        }
    }
    
    //Controla que se disminuya en 100Hp por stat en 0
    public void Minus() {
        if (this.feed == 0 && "t".equals(this.ifFeed)) {
            this.hp = this.hp - 100;
            this.ifFeed = "f";
            if (this.hp < 0) {
                this.hp = 0;
            }
        }

        if ("t".equals(this.ifClean) && this.clean == 0) {
            this.hp = this.hp - 100;
            this.ifClean = "f";
            if (this.hp < 0) {
                this.hp = 0;
            }
        }

        if (this.happiness == 0 && "t".equals(this.ifHappiness)) {
            this.hp = this.hp - 100;
            this.ifHappiness = "f";
            if (this.hp < 0) {
                this.hp = 0;
            }
        }
    }
    
    //Aumento del animo de la mascota
    public void Cheerup() {
        this.happiness++;
        if (this.happiness > 10) {
            this.happiness = 10;
        }
    }

    //Limpieza
    public void Clean() {
        this.clean++;
        if (this.clean > 10) {
            this.clean = 10;
        }
    }
    
}
