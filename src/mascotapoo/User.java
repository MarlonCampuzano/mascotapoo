/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author gaby
 */
public class User {
    String userName;
    Mascota animal;
    Food[] food = new Food[10];

    public String getUser() {
        return userName;
    }

    public User() {

        initMascota();
        initFood();

    }

    //DEVUELVE UN OBJETO JSON A PARTIR DE UNA URL
    private JSONObject retrieveJSON(String url) {
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(new FileReader(url));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | org.json.simple.parser.ParseException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject jsonObject = (JSONObject) obj;
        return jsonObject;
    }

    private void initMascota() {
        JSONObject jsonObject = retrieveJSON("user-save.json");
        if ("vivo".equals((String) jsonObject.get("state"))) {
            ResumeMascota();
         
        } else {
            DefaultMascota();
        }
    }

    private void initFood() {
        JSONObject jsonObject = (JSONObject) retrieveJSON("food-config.json");

        int i = 0;
        while (jsonObject.get(String.valueOf(i)) != null) {
            String data = jsonObject.get(String.valueOf(i)).toString();
      
            food[i] = new Food();

            //SEPARAR DATA DEL JSON FOOD Y LLENADO DEL ARRAY
            StringTokenizer st = new StringTokenizer(data);
            int op = 0;
            while (st.hasMoreElements()) {
                String token = st.nextElement().toString();
          
                switch (op) {
                    case 0:
                        food[i].setName(token);
                        break;

                    case 1:
                        food[i].setFoodVal(Integer.parseInt(token));
                        break;
                    case 2:
                        food[i].setMoneyVal(Integer.parseInt(token));
                        break;
                }
                op++;
            }

            i++;

        }

    }

    //ALIMENTA A LA MASCOTA
    public void Alimentar() {
        List<String> choices = new ArrayList<>();
        //MODIFICAR SI SE AUMENTA LA LISTA DE ALIMENTOS
        for (int i = 0; i < 3; i++) {

            choices.add(food[i].getName());

            int a = food.length;
            System.out.println(a);

        }

        ChoiceDialog<String> dialog = new ChoiceDialog<>("", choices);
        dialog.setTitle("Escoge un alimento");
        dialog.setHeaderText("Alimentar");
        dialog.setContentText("Alimento:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String op = result.get();
            System.out.println("Escogiste: " + op);
            int k = 0;
            while (!op.equals(food[k].getName())) {
                k++;
            }
            if (food[k].getMoneyVal() > animal.getMoney()) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("ATENCION!");
                alert.setHeaderText("No se pudo alimentar a la mascota");
                alert.setContentText("Consigue unas monedas mas para alimentar a tu mascota");

                alert.showAndWait();
            } else {
                animal.setMoney(animal.getMoney() - food[k].getMoneyVal());
                animal.setFeed(animal.getFeed() + food[k].getFoodVal());
                if (animal.getFeed() > 10) {
                    animal.setFeed(10);
                }
            }

        }
    }

    //GUARDA LA PARTIDA AL CERRAR
    void Guardar() {

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat days = new SimpleDateFormat("yyyy MM dd");
        SimpleDateFormat hours = new SimpleDateFormat("kk mm ss");

        //PREPARANDO DATOS A GUARDAR
        String state = state();
        String name = this.animal.getName();
        int age = (int) this.animal.getAge();
        String b_date = this.animal.getB_date();
        String b_hour = this.animal.getB_hour();
        int hp = (int) this.animal.getHp();
        int feed = (int) this.animal.getFeed();
        int happiness = (int) this.animal.getHappiness();
        int money = (int) this.animal.getMoney();
        int clean = (int) this.animal.getClean();
        String e_date = days.format(date);
        String e_hour = hours.format(date);

        String ifFeed = this.animal.getIfFeed();
        String ifClean = this.animal.getIfClean();
        String ifHappiness = this.animal.getIfHappiness();

        FileWriter escritor = null;

        try {
            String guardar = "{"
                    + quote("user") + ":" + quote(this.userName) + ","
                    + quote("state") + ":" + quote(state) + ","
                    + quote("name") + ":" + quote(name) + ","
                    + quote("age") + ":" + quote(String.valueOf(age)) + ","
                    + quote("b_date") + ":" + quote(b_date) + ","
                    + quote("b_hour") + ":" + quote(b_hour) + ","
                    + quote("hp") + ":" + quote(String.valueOf(hp)) + ","
                    + quote("feed") + ":" + quote(String.valueOf(feed)) + ","
                    + quote("happiness") + ":" + quote(String.valueOf(happiness)) + ","
                    + quote("money") + ":" + quote(String.valueOf(money)) + ","
                    + quote("clean") + ":" + quote(String.valueOf(clean)) + ","
                    + quote("e_date") + ":" + quote(e_date) + ","
                    + quote("e_hour") + ":" + quote(String.valueOf(e_hour)) + ","
                    + quote("ifFeed") + ":" + quote(ifFeed) + ","
                    + quote("ifClean") + ":" + quote(ifClean) + ","
                    + quote("ifHappiness") + ":" + quote(ifHappiness)
                    + "}";
            escritor = new FileWriter("user-save.json");
            for (int i = 0; i < guardar.length(); i++) {
                escritor.write(guardar.charAt(i));
            }
            escritor.close();
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                escritor.close();
            } catch (IOException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //DEVUELVE UN STRING ENTRE COMILLAS
    private String quote(String str) {
        return (char) 34 + str + (char) 34;
    }

    //CONTROLA EL STADO DE LA MASCOTA VIVO O MUERTO
    private String state() {
        if (this.animal.getHp() == 0) {
            return "muerto";
        } else {
            return "vivo";
        }
    }

    //INICIA UNA MASCOTA DE 0 CONC ONFIGURACIONES POR DEFAULT
    private void DefaultMascota() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Ingrese nombre de usuario");
        dialog.setContentText("Usuario");
        Optional<String> strUser = dialog.showAndWait();
        if (strUser.isPresent()) {
            this.userName = strUser.get();
        }

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat days = new SimpleDateFormat("yyyy MM dd");
        SimpleDateFormat hours = new SimpleDateFormat("kk mm ss");
        String b_date = days.format(date);
        String b_hour = hours.format(date);
        System.out.println(b_hour);

        String name = "";

        dialog = new TextInputDialog("");
        dialog.setTitle("Ingrese nombre de mascota");
        dialog.setContentText("Mascota");
        strUser = dialog.showAndWait();
        if (strUser.isPresent()) {
            name = strUser.get();
        }

        JSONObject jsonObject = retrieveJSON("animal-config.json");

        double age = Integer.parseInt((String) jsonObject.get("age"));
        double hp = Integer.parseInt((String) jsonObject.get("hp"));
        double feed = Integer.parseInt((String) jsonObject.get("feed"));
        double happiness = Integer.parseInt((String) jsonObject.get("happiness"));
        double money = Integer.parseInt((String) jsonObject.get("money"));
        double clean = Integer.parseInt((String) jsonObject.get("clean"));

        this.animal = new Mascota(name, b_date, b_hour, age, hp, feed, happiness, money, clean);

        System.out.println("Bienvenido " + this.userName);
        System.out.println("Mascota creada con exito");
        System.out.println("Nombre " + this.animal.getName());
        System.out.println("Fecha de nacimiento  " + this.animal.getB_date());
        System.out.println("Hora de nacimiento  " + this.animal.getB_hour());
        System.out.println("Edad " + this.animal.getAge());
        System.out.println("Total de Vida " + this.animal.getHp());
        System.out.println("Alimentacion " + this.animal.getFeed());
        System.out.println("Animo " + this.animal.getHappiness());
        System.out.println("Limpieza " + this.animal.getClean());
    }

    //INICIA Y ACTUALIZA UNA MASCOTA DESDE UN ARCHIVO JSON GUARDADO
    private void ResumeMascota() {

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat days = new SimpleDateFormat("yyyy MM dd");
        SimpleDateFormat hours = new SimpleDateFormat("kk mm ss");
        String a_date = days.format(date);
        String a_hour = hours.format(date);

        JSONObject jsonObject = retrieveJSON("user-save.json");
        this.userName = (String) jsonObject.get("user");
        String name = (String) jsonObject.get("name");
        String b_date = (String) jsonObject.get("b_date");
        String b_hour = (String) jsonObject.get("b_hour");
        String e_date = jsonObject.get("e_date").toString();
        String e_hour = jsonObject.get("e_hour").toString();
        double age = Integer.parseInt((String) jsonObject.get("age"));
        double hp = Integer.parseInt((String) jsonObject.get("hp"));
        double feed = Integer.parseInt((String) jsonObject.get("feed"));
        double happiness = Integer.parseInt((String) jsonObject.get("happiness"));
        double money = Integer.parseInt((String) jsonObject.get("money"));
        double clean = Integer.parseInt((String) jsonObject.get("clean"));

        this.animal = new Mascota(name, b_date, b_hour, age, hp, feed, happiness, money, clean);

        this.animal.setIfFeed(jsonObject.get("ifFeed").toString());
        this.animal.setIfClean(jsonObject.get("ifClean").toString());
        this.animal.setIfHappiness(jsonObject.get("ifHappiness").toString());

        StringTokenizer ah = new StringTokenizer(a_hour);
        StringTokenizer ad = new StringTokenizer(a_date);
        StringTokenizer eh = new StringTokenizer(e_hour);
        StringTokenizer ed = new StringTokenizer(e_date);
        int op = 0;
        int[] hora = new int[3];
        int[] fecha = new int[3];
        while (ah.hasMoreElements()
                && ad.hasMoreElements()
                && eh.hasMoreElements()
                && ed.hasMoreElements()) {
            int ahToken = Integer.parseInt(ah.nextElement().toString());
            int adToken = Integer.parseInt(ad.nextElement().toString());
            int ehToken = Integer.parseInt(eh.nextElement().toString());
            int edToken = Integer.parseInt(ed.nextElement().toString());
            int d = adToken - edToken;
            int h = ahToken - ehToken;
            System.out.println(ahToken+" "+ehToken+" "+adToken+" "+edToken);
            switch (op) {
                //TRATAMIENTO DE ANIOS EN FECHA, HORAS EN HORARIO
                case 0:
                    if (adToken > edToken) {
                        this.animal.setAge(1000000);
                        this.animal.setHp(0);
                        this.animal.setClean(0);
                        this.animal.setFeed(0);
                        this.animal.setHappiness(0);
                        this.animal.Minus();
                    } else {
                        if (ahToken > ehToken) {

                            System.out.println(ahToken+" "+h);
                            this.animal.setAge((int) (this.animal.getAge() + h));
                            this.animal.setHp((int) (this.animal.getHp() - h));
                            if (this.animal.getHp() < 0) {
                                this.animal.setHp(0);
                            }
                            this.animal.setClean(0);
                            this.animal.setFeed(0);
                            this.animal.setHappiness(0);
                            this.animal.Minus();
                        }
                    }
                    break;
                //TRATAMIENTO DE MESES EN FECHA, MINUTOS EN HORAS
                case 1:
                    if (adToken > edToken) {
                        this.animal.setAge((int) (this.animal.getAge() + d));
                        this.animal.setHp(0);
                        this.animal.setClean(0);
                        this.animal.setFeed(0);
                        this.animal.setHappiness(0);
                        this.animal.Minus();
                    } else {
                        this.animal.setClean((int) (this.animal.getClean() - h));
                        this.animal.setFeed((int) (this.animal.getFeed() - h));
                        this.animal.setHappiness((int) (this.animal.getHappiness() - h));
                        this.animal.Minus();
                    }
                    break;
                //TRATAMIENTO DE DIAS EN FECHA, SEGUNDOS EN HORAS                //TRATAMIENTO DE DIAS EN FECHA, SEGUNDOS EN HORAS                //TRATAMIENTO DE DIAS EN FECHA, SEGUNDOS EN HORAS                //TRATAMIENTO DE DIAS EN FECHA, SEGUNDOS EN HORAS

                case 2:
                    if (adToken > edToken) {
                        this.animal.setAge((int) (this.animal.getAge() + d));
                        this.animal.setHp((int) (this.animal.getAge() - d));
                        this.animal.setClean(0);
                        this.animal.setFeed(0);
                        this.animal.setHappiness(0);
                        this.animal.Minus();
                    }
                    break;
            }

            op++;
        }

    }
}
