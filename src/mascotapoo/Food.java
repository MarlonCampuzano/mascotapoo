/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

/**
 *
 * @author gaby
 */
public class Food {
    private String name;
    private int foodVal;
    private int moneyVal;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFoodVal() {
        return foodVal;
    }

    public void setFoodVal(int foodVal) {
        this.foodVal = foodVal;
    }

    public int getMoneyVal() {
        return moneyVal;
    }

    public void setMoneyVal(int moneyVal) {
        this.moneyVal = moneyVal;
    }
    
    
    
}
