/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mascotapoo;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import static javafx.geometry.Pos.CENTER;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author usuario
 */
public class MascotaPoo extends Application {
    
    @Override
    public void start(Stage primaryStage) {

        User user = new User();

        //ETIQUETAS DE USUARIO Y ESTADO DE MASCOTA
        Label labelUser = new Label();
        labelUser.setText("Usuario: " + user.getUser());
        Label labelPet = new Label();
        labelPet.setText("Mascota: " + user.animal.getName());
        Label labelAge = new Label();
        labelAge.setText("Edad: " + user.animal.getAge());
        Label labelMoney = new Label();
        labelMoney.setText("Dinero: $" + user.animal.getMoney());
        Label labelFeed = new Label();
        labelFeed.setText("Alimentacion: " + user.animal.getFeed());
        Label labelHappiness = new Label();
        labelHappiness.setText("Animo: " + user.animal.getHappiness());
        Label labelClean = new Label();
        labelClean.setText("Limpieza: " + user.animal.getClean());
        Label labelHp = new Label();
        labelHp.setText("Tiempo de vida: " + user.animal.getHp());

        //BARRAS DE ESTADO DE MASCOTA
        ProgressBar barFeed = new ProgressBar(10.10);
        ProgressBar barHappiness = new ProgressBar(10.10);
        ProgressBar barClean = new ProgressBar(10.10);
        ProgressBar barHp = new ProgressBar(500.500);

        //CARGANDO IMAGEN DE CUARTO
        Image bgRoom = new Image("room10.jpg");
        Image bgRoom9 = new Image("room9.jpg");
        Image bgRoom8 = new Image("room8.jpg");
        Image bgRoom7 = new Image("room7.jpg");
        Image bgRoom6 = new Image("room6.jpg");
        Image bgRoom5 = new Image("room5.jpg");
        Image bgRoom4 = new Image("room4.jpg");
        Image bgRoom3 = new Image("room3.jpg");
        Image bgRoom2 = new Image("room2.jpg");
        Image bgRoom1 = new Image("room1.jpg");
        Image bgRoom0 = new Image("room0.jpg");
        Image petImg = new Image("pet.png");

        BorderPane borderPane = new BorderPane();
        FlowPane innerBorder = new FlowPane();
        FlowPane rMenu = new FlowPane(Orientation.VERTICAL);

        ImageView pet = new ImageView();

        Button btnFeed = new Button();
        btnFeed.setText("Alimentar");
        btnFeed.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                user.Alimentar();
                System.out.println("Hello World!");
            }
        });
        // METODOS DE BOTONES DE ACCION
        Button btnPlay = new Button();
        btnPlay.setText("Jugar");
        btnPlay.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                try {
                    user.animal.Cheerup();
                    game.Game juego = new game.Game();
                    Stage gameStage = new Stage();
                    
                    gameStage.setOnCloseRequest(e->{
                        try {
                            user.animal.setMoney(juego.getMoneyBank()+user.animal.getMoney());
                            juego.stop();
                        } catch (Exception ex) {
                            Logger.getLogger(MascotaPoo.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                    juego.start(gameStage);
                    
                } catch (Exception ex) {
                    Logger.getLogger(MascotaPoo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        Button btnClean = new Button();
        btnClean.setText("Limpiar");
        btnClean.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                user.animal.Clean();
            }
        });

        pet.setImage(petImg);
        pet.setTranslateX(30);
        pet.setTranslateY(200);

        innerBorder.setBackground(new Background(new BackgroundImage(bgRoom, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
        innerBorder.getChildren().add(pet);

        //AGREGAMOS TODOS LOS ELEMENTOS THE STATS
        rMenu.getChildren().addAll(
                labelUser,
                labelPet,
                labelAge,
                labelMoney,
                labelFeed,
                barFeed,
                labelHappiness,
                barHappiness,
                labelClean,
                barClean,
                labelHp,
                barHp,
                btnFeed,
                btnPlay,
                btnClean
        );

        //CONFIGURACIONES
        rMenu.setPadding(new Insets(5, 5, 5, 5));
        rMenu.setVgap(10);
        rMenu.setHgap(30);
        rMenu.setAlignment(CENTER);

        borderPane.setCenter(innerBorder);
        borderPane.setRight(rMenu);

        StackPane root = new StackPane();
        root.getChildren().add(borderPane);

        Scene scene = new Scene(root, 853, 600);

        primaryStage.setTitle("Cuida a la mascota");
        primaryStage.setScene(scene);
        primaryStage.show();

        //FUNCIONES AL MOMENTO DE CERRAR
        primaryStage.setOnCloseRequest(event -> {
            user.Guardar();
            System.exit(0);
        });
        timer(user);

        //MODIFICA LA INTERFAZ GRAFICA EN EL TRANSCURSO DEL TIEMPO
        Timeline timeline = new Timeline(
                // 0.017 ~= 60 FPS
                new KeyFrame(Duration.seconds(.517), new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent ae) {

                        //ACTUALIZA LAS BARRAS DE PROGRESO EN EL TIEMPO
                        ActualizarBAR();

                        //ACTUALIZA LAS LABELS
                        ActualizarLB();

                        //ACTUALIZA EL CUARTO RESPECTO A LA SUCIEDAD
                        ActualizarBG();

                    }
                       //CAMBIA EL BG DE LA MASCOTA CON RESPECTO A LA LIMPIEZA
                    private void ActualizarBG() {
                        switch ((int) user.animal.getClean()) {
                            case 0:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom0, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 1:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom1, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 2:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom2, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 3:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom3, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 4:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom4, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 5:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom5, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 6:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom6, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 7:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom7, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 8:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom8, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 9:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom9, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                            case 10:
                                innerBorder.setBackground(new Background(new BackgroundImage(bgRoom, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
                                break;
                        }
                    }
                    //ACTUALIZA LAS ETIQUETAS 
                    private void ActualizarLB() {
                        labelAge.setText("Edad: " + user.animal.getAge());
                        labelMoney.setText("Dinero: $" + user.animal.getMoney());
                        labelFeed.setText("Alimentacion: " + user.animal.getFeed());
                        labelHappiness.setText("Animo: " + user.animal.getHappiness());
                        labelClean.setText("Limpieza: " + user.animal.getClean());
                        labelHp.setText("Tiempo de vida: " + user.animal.getHp());

                    }
                    //ACTUALIZA LAS BARRAS DE STATS
                    private void ActualizarBAR() {
                        barFeed.setProgress(user.animal.getFeed() / 10);
                        barHappiness.setProgress(user.animal.getHappiness() / 10);
                        barClean.setProgress(user.animal.getClean() / 10);
                        barHp.setProgress(user.animal.getHp() / 500);
                    }
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
//CONTROLA EL TIEMPO DE EJECUCION DE LOS METODOS DURANTE LA VIDA DE LA MASCOTA

    private static void timer(User user) {

        Task t = new Task() {
            @Override
            protected Object call() throws Exception {
                int hour = 0;
                int min = 0;
                int sec = 0;

                while (user.animal.getHp() != 0) {

                    for (min = 0; min < 60; min++) {
                        for (sec = 0; sec < 60; sec++) {
                            //System.out.println(hour + ":" + min + ":" + sec);
                            //crono.setText(hour + ":" + min  + ":" + sec);
                            delaySec();
                        }
                        user.animal.DecreaseStats();
                        user.animal.Minus();
                        Suciedad(min);
                    }
                    hour++;
                    user.animal.Birthday();

                }
                return true;
            }
            //CONTROLA EL TIEMPO ALEATORIO DE SUCIEDAD
            private void Suciedad(int min) {
                int n = 4, numero = (int) (Math.random() * n) + 2;
                System.out.println(numero);
                if (min % numero == 0) {
                    user.animal.setClean((int) user.animal.getClean() - 1);
                }
                if (user.animal.getClean() < 0) {
                    user.animal.setClean(0);
                }
            }

        };

        new Thread(t).start();

    }

    //FUNCION PARA EL THREAD PARA SIMULAR SEGUNDOS THREAD.SLEEP(1000)
    private static void delaySec() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }

}
